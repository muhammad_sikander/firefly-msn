<?php

namespace FireflyIII\Http\Controllers;
use Carbon\Carbon;
use FireflyIII\Paypal;
use FireflyIII\Models\Account;
use FireflyIII\Models\TransactionJournal;
use FireflyIII\Models\Transaction;
use FireflyIII\Models\TransactionType;
use FireflyIII\Support\Http\Controllers\BasicDataSupport;
use Illuminate\Pagination\LengthAwarePaginator;
use FireflyIII\Repositories\Journal\JournalRepositoryInterface;
use Illuminate\Http\Request;
use Naif\LaravelPayPal\LaravelPayPal;
use FireflyIII\Http\Controllers\PaypalAccount;
use FireflyIII\Repositories\Account\AccountRepositoryInterface;
use Session;

class PaypalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $paypal1 = new LaravelPayPal();

        $balance = $paypal1->getBalance();
        
        $paypal = Paypal::first(); 
        
        $message1 = Session::get('message');
        if($message1 != null || $message1 !=""){

            $message = 'invalid';
        } else {

            $message = $balance['balance']['ACK'];
        }
       
        return view('paypal',compact('paypal','message'));
      

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, JournalRepositoryInterface $repository, AccountRepositoryInterface $repository1)
    {
        //
       $paypal1 = new LaravelPayPal();
        $balance = $paypal1->getBalance();

        if($balance['balance']['ACK'] == "Success") {

        $GetAccount =$paypal1->getAccount();
        $transactions =$paypal1->getTransactions();
        
        $trans = $transactions['transactions'];
        $paypalAccount = new PaypalAccount();
        
        $store = $paypalAccount->store1($repository1);
        

        if($store['response']['sucess'] == "Success") {
           
           $account = Account::where('name',$store['response']['PAL'])->get();
           $accountId = $account[0]['id'];
        
        } elseif ($GetAccount) {

           $account = Account::where('name',$GetAccount['account']['PAL'])->get();
           $accountId = $account[0]['id'];
        }
        $trans1 = array_reverse( $transactions['transactions'] );

        if($trans1 == null || $trans1 =="") {

          $trans1 = "";
       
        } else {

        $PaypalType = '';
        $amt = '';
        

        foreach ($trans1 as $dataT){
        $record = "";
         
        $Transaction = TransactionJournal::where('transaction_paypal_id',$dataT['transaction_id'])->withTrashed()->first();
         
        if( !empty($Transaction )) {

          $record = $Transaction;
          $Transaction = null;
          
          
        } else {

          $record = "";
          
      
        } 
       if( empty($record)) {
          
          if($dataT['amt'] == "" || $dataT['amt'] == null) {

              $amt = number_format('0',2);
                

        } else {

                $amt = $dataT['amt'];
                
        }

         if($dataT['amt'] > 0) {

             $PaypalType = 'deposit';

           } else {

             $PaypalType = 'withdrawal';

           }
           if($PaypalType == 'deposit'){
              $destination_id = $accountId;
              $source_id = null;
           }else{
              $source_id = $accountId;
              $destination_id = null;
           }
        $PaypalData = array(
              "type" => $PaypalType,
              "date" =>  Carbon::parse($dataT['timestamp']),
              "tags" => array(0 => $GetAccount['account']['PAL']),
              "user" => 1,
              "interest_date" => null,
              "book_date" => null,
              "process_date" => null,
              "due_date" => null,
              "payment_date" => null,
              "invoice_date" => null,
              "internal_reference" => "",
              "notes" => "",
              "transaction_paypal_id" => $dataT['transaction_id'],
              "description" => $dataT['name'],
              "piggy_bank_id" => 0,
              "piggy_bank_name" => null,
              "bill_id" => null,
              "bill_name" => null,
              "original-source" => "gui-v4.7.15",
              "transactions" => array(0 => array(
                          "currency_id" => 7,
                          "currency_code" => null,
                          "description" => null,
                          "amount" => $amt,
                          "budget_id" => 0,
                          "budget_name" => null,
                          "category_id" => null,
                          "category_name" => "",
                          "source_id" => $source_id,
                          "source_name" => "",
                          "destination_id" => $destination_id,
                          "destination_name" => "",
                          "foreign_currency_id" => null,
                          "foreign_currency_code" => null,
                          "foreign_amount" => null,
                          "reconciled" => false,
                          "identifier" => 0
              ))
        );
         
         $journal       = $repository->store($PaypalData);
        
      } }
    }
      }
        $message1 = Session::get('message');
        if($message1 != null || $message1 !=""){

            $message = 'invalid';
        } else {

            $message = $balance['balance']['ACK'];
        }
       
        $paypal = Paypal::first();    

        // Get current page form url e.x. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
 
        // Create a new Laravel collection from the array data
        $itemCollection = collect($trans);
 
        // Define how many items we want to be visible in each page
        $perPage = 50;
 
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
 
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
 
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('paypal',compact('paypal','message','paginatedItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            $post = new Paypal();
            
            $post->username = $request->username;
            $post->password = $request->password;
            $post->signature = $request->signature;
            
            $post->save();
            $redirect = redirect(route('paypal.index'));
            return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  \FireflyIII\Paypal  $paypal
     * @return \Illuminate\Http\Response
     */
    public function show(Paypal $paypal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \FireflyIII\Paypal  $paypal
     * @return \Illuminate\Http\Response
     */
    public function edit(Paypal $paypal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \FireflyIII\Paypal  $paypal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
          $fields = 
            array(
       
                'METHOD'    => 'GetBalance',
                'VERSION'   => 47,
                'USER'      => $request->username,
                'PWD'       => $request->password,
                'SIGNATURE' => $request->signature
                   );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (!$response) {
            throw new Exception('Failed to contact PayPal API: ' . curl_error($ch) . ' (Error No. ' . curl_errno($ch) . ')');
        }
        curl_close($ch);
        parse_str($response, $result);
        $message = $result['ACK'];
        if($message == "Failure") {
        $redirect = redirect(route('index'))->with(['message' => $message]);
        return $redirect;
        } else {

        $post = paypal::find($request->id);
        $post->update($request->all());
        $redirect = redirect(route('paypal.index'));
        return $redirect;

        }
    

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \FireflyIII\Paypal  $paypal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paypal $paypal)
    {
        //
    }
}
