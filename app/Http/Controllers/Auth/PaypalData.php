<?php 
namespace FireflyIII\Http\Controllers\Auth;

use Carbon\Carbon;
use FireflyIII\Paypal;
use FireflyIII\Models\Account;
use FireflyIII\Models\TransactionJournal;
use FireflyIII\Models\Transaction;
use FireflyIII\Models\TransactionType;
use FireflyIII\Support\Http\Controllers\BasicDataSupport;
use Illuminate\Pagination\LengthAwarePaginator;
use FireflyIII\Repositories\Journal\JournalRepositoryInterface;
use Illuminate\Http\Request;
use Naif\LaravelPayPal\LaravelPayPal;


class PaypalData  {


public function PaypalLog (JournalRepositoryInterface $repository) {


 		$account = Account::where('name','paypal')->get();
        $accountId = $account[0]['id'];
        
        $paypal1 = new LaravelPayPal();
        
        $transactions =$paypal1->getTransactions();
       
        if(!empty($transactions)) {
        $trans = array_reverse( $transactions['transactions'] );

        $PaypalType = '';
        $amt = '';
        foreach ($trans as $dataT){
        
        $Transaction = TransactionJournal::withTrashed('transaction_paypal_id',$dataT['transaction_id'])->first();
          
           if( empty($Transaction)  ) {
        
            if($dataT['amt'] == "" || $dataT['amt'] == null) {

                $amt = number_format('0',2);
                

            } else {

                $amt = $dataT['amt'];
                
            }

         if($dataT['amt'] > 0) {

             $PaypalType = 'deposit';

           } else {

             $PaypalType = 'withdrawal';

           }
           if($PaypalType == 'deposit'){
              $destination_id = $accountId;
              $source_id = null;
           }else{
              $source_id = $accountId;
              $destination_id = null;
           }
        $PaypalData = array(
             "type" => $PaypalType,
             "date" =>  Carbon::parse($dataT['timestamp']),
              "tags" => array(0 => "paypal"),
              "user" => 1,
              "interest_date" => null,
              "book_date" => null,
              "process_date" => null,
              "due_date" => null,
              "payment_date" => null,
              "invoice_date" => null,
              "internal_reference" => "",
              "notes" => "",
              "transaction_paypal_id" => $dataT['transaction_id'],
              "description" => $dataT['name'],
              "piggy_bank_id" => 0,
              "piggy_bank_name" => null,
              "bill_id" => null,
              "bill_name" => null,
              "original-source" => "gui-v4.7.15",
              "transactions" => array(0 => array(
                          "currency_id" => 7,
                          "currency_code" => null,
                          "description" => null,
                          "amount" => $amt,
                          "budget_id" => 0,
                          "budget_name" => null,
                          "category_id" => null,
                          "category_name" => "",
                          "source_id" => $source_id,
                          "source_name" => "",
                          "destination_id" => $destination_id,
                          "destination_name" => "",
                          "foreign_currency_id" => null,
                          "foreign_currency_code" => null,
                          "foreign_amount" => null,
                          "reconciled" => false,
                          "identifier" => 0
              ))
        );
        
         $journal       = $repository->store($PaypalData);
       }
        }
        	}

}
}