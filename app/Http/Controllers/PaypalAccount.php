<?php 

namespace FireflyIII\Http\Controllers;
use Carbon\Carbon;
use FireflyIII\Paypal;
use FireflyIII\Http\Controllers\Controller;
use FireflyIII\Http\Requests\AccountFormRequest;
use FireflyIII\Models\AccountType;
use FireflyIII\Repositories\Account\AccountRepositoryInterface;
use Illuminate\Http\Request;
use Naif\LaravelPayPal\LaravelPayPal;
use Log;
class PaypalAccount extends Controller
{
 public function store1($repository1)
    {

    	$paypal1 = new LaravelPayPal();
        $data    = $paypal1->getAccount();
        $balance    = $paypal1->getBalance();
       if( $data['account']['ACK'] != "Failure"){
        $accountData = array(

        	  "name" => $data['account']['PAL'],
			  "active" => true,
			  "accountType" => "asset",
			  "account_type_id" => 0,
			  "currency_id" => 7,
			  "virtualBalance" => $balance['balance']['L_AMT0'],
			  "iban" => "",
			  "BIC" => "",
			  "accountNumber" => "",
			  "accountRole" => "defaultAsset",
			  "openingBalance" => $balance['balance']['L_AMT0'],
			  "openingBalanceDate" => "",
			  "ccType" => "",
			  "ccMonthlyPaymentDate" => "",
			  "notes" => "",
			  "interest" => "",
			  "interest_period" => "",
			  "include_net_worth" => "1"


        );
        $account = $repository1->store($accountData);
    
        if($account) {

        	$sucess['response']['PAL'] = $data['account']['PAL']; 
        	$sucess['response']['sucess'] = "Success"; 
        }else {

        	$sucess = "Failed";
        }
    } else {

    	$sucess['response']['sucess'] = "Failed";
    }
        return $sucess;
    
    }

}