<?php

namespace FireflyIII\Http\Controllers;

use Carbon\Carbon;
use FireflyIII\Exceptions\FireflyException;
use FireflyIII\Helpers\Collector\TransactionCollectorInterface;
use FireflyIII\Helpers\Filter\CountAttachmentsFilter;
use FireflyIII\Helpers\Filter\InternalTransferFilter;
use FireflyIII\Helpers\Filter\SplitIndicatorFilter;
use FireflyIII\Models\Attachment;
use FireflyIII\Models\Transaction;
use FireflyIII\Models\TransactionJournal;
use FireflyIII\Models\TransactionType;
use FireflyIII\Repositories\Attachment\AttachmentRepositoryInterface;
use FireflyIII\Repositories\Journal\JournalRepositoryInterface;
use FireflyIII\Repositories\LinkType\LinkTypeRepositoryInterface;
use FireflyIII\Support\Http\Controllers\ModelInformation;
use FireflyIII\Support\Http\Controllers\PeriodOverview;
use FireflyIII\Transformers\TransactionTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Log;
use Symfony\Component\HttpFoundation\ParameterBag;
use View;

class AllTransactions extends Controller
{

	 use ModelInformation, PeriodOverview;
    /** @var AttachmentRepositoryInterface */
    private $attachmentRepository;
    /** @var JournalRepositoryInterface Journals and transactions overview */
    private $repository;

    /**
     * TransactionController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware(
            function ($request, $next) {
                app('view')->share('title', (string)trans('firefly.transactions'));
                app('view')->share('mainTitleIcon', 'fa-repeat');
                $this->repository           = app(JournalRepositoryInterface::class);
                $this->attachmentRepository = app(AttachmentRepositoryInterface::class);

                return $next($request);
            }
        );
    }
    //
     public function index(Request $request){

     	$what = 'all';
     	$subTitleIcon = config('firefly.transactionIconsByWhat.' . $what);
        $types        = config('firefly.transactionTypesByWhat.' . $what);
        $page         =(int)$request->get('page');
        $pageSize     = (int)app('preferences')->get('listPageSize', 50)->data;
        $path         = route('AllTransactions');
        $first        = $this->repository->firstNull();
        $start        = null === $first ? new Carbon : $first->date;
        $end          = new Carbon;
        $subTitle     = '';

        /** @var TransactionCollectorInterface $collector */
        $collector = app(TransactionCollectorInterface::class);
        $collector->setAllAssetAccounts()->setRange($start, $end)
                  ->setTypes($types)->setLimit($pageSize)->setPage($page)->withOpposingAccount()
                  ->withBudgetInformation()->withCategoryInformation();
        $collector->removeFilter(InternalTransferFilter::class);
        $collector->addFilter(SplitIndicatorFilter::class);
        $collector->addFilter(CountAttachmentsFilter::class);
        $transactions = $collector->getPaginatedTransactions();
        $transactions->setPath($path);
     	
     	return view('AllTransactions',compact('subTitle', 'what', 'subTitleIcon', 'transactions', 'start', 'end'));
     }
}
