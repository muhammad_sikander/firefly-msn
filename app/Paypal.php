<?php

namespace FireflyIII;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model
{
    //
     protected $fillable = ['username', 'password', 'signature'];
}
